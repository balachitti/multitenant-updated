const db = require(`./controller/qureies`)
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const port = process.env.PORT 
const app = express()
//const db = require('./controller')
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
app.use(cors())
app.get('/', (request, response) => {
    response.json(`backend is ready`)
    })
app.get('/school/getschool',db.displaySchool)
app.post('/school/:schoolname',db.createSchool)
app.post('/school/:schoolname/addregister',db.createRegister)
app.post('/school/:schoolname/addstudent',db.addStudent)
app.get('/school/:schoolname/getallstudent',db.displayStudentdata)
app.get('/school/:schoolname/getstudent/:id',db.displayStudent)
app.put('/school/:schoolname/updatestudent/:id',db.updateStudentdata)
app.delete('/school/:schoolname/deletestudent/:id',db.deleteStudent)
app.get('/school/:schoolname/backup',db.backup)
app.get('/school/:schoolname/:id/backup',db.backupStudent)
app.delete('/school/:schoolname/dropschool',db.dropschool)
app.get('/school/:schoolname/getbackupdata',db.displayBackupdata)
app.listen(port, () => {
    console.log(`App running on port ${port}.`);
})
