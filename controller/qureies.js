require(`../config/config`)


const { Client } = require('pg')
const connectionString = 'postgres://postgres:vino@localhost:5432/db'; //this uri conatins postgres://user:password@host:port/databasename
const client = new Client({
    connectionString: connectionString
});
client.connect().then(()=>console.log(`connection succeed`))
.catch(() => console.error(`error in connecting psql check the URI`))


createSchool = (request, response) => {
    const name = request.params.schoolname  
    //console.log(name)
    //console.log(typeof(name))
    const createsqlschool = `CREATE SCHEMA IF NOT EXISTS ${name};`
    client.query( createsqlschool, (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(`schema created sucessfully`)
      console.log(`schema created sucessfully`)
    })
  }
createRegister = (request,response) =>{
    // const tablename = request.params.tablename
    const name = request.params.schoolname
    //console.log(name)
    const createsqlreg = `CREATE TABLE IF NOT EXISTS ${name}.studentdetails(student_id serial PRIMARY KEY, student_name varchar(30),location varchar(20), languages text);`
      client.query(createsqlreg,(error,results)=>{
      if (error) {
        response.status(500).json('error in creating table')
        throw error
      }
      response.status(200).json(`table created sucessfully`)
      console.log(`table created sucessfully`)
    })
  }
  addStudent = (request,response) =>{
    const name = request.params.schoolname
     //const student_id = request.body.id
     const student_name = request.body.student_name
     const location = request.body.location
     const languages = request.body.languages
     console.log(languages);
    const insertstudent =  `INSERT INTO ${name}.studentdetails ( student_name , location , languages )  VALUES ('${student_name}','${location}','${languages}'); `
    client.query(insertstudent,(error,results) => {
      if (error){
        response.status(500).json('error in inserting table')
        throw error
      }
      response.status(200).json(`sucessfully inserted`)
    })
 }
displaySchool = (request,response) =>{
  console.log("list")
  const display = `SELECT s.nspname AS schoolname from pg_catalog.pg_namespace s where nspname not in ('information_schema', 'pg_catalog','public')
 and nspname not like 'pg_toast%' and nspname not like 'pg_temp_%';`
client.query(display,(error,results) =>{
  if(error) { 
    response.status(500).json('error in displaying data')
    throw error
    
    }
    response.status(200).json(results.rows)
})
}
displayStudentdata = (request,response) =>{
  console.log("student data")
  const name = request.params.schoolname
    const displaystudent = `SELECT * FROM ${name}.studentdetails`
    client.query(displaystudent,(error,results) =>{
      if(error){
        response.status(500).json('error in displaying table')
        throw error
        
      }
      response.status(200).json(results.rows)
    })

}
displayStudent = (request,response) =>{
  console.log("student data id")
  const name = request.params.schoolname
  const id = request.params.id
    const displaystudent = `SELECT * FROM ${name}.studentdetails WHERE student_id = ${id}`
    client.query(displaystudent,(error,results) =>{
      if(error){
        response.status(500).json('error in creating table')
        throw error
        
      }
      response.status(200).json(results.rows)
    })

}
displayBackupdata  = (request,response) =>{
  console.log("student data")
  const displaybackup = `SELECT * FROM public.backup`
    client.query(displaybackup,(error,results) =>{
      if(error){
        response.status(500).json('error in displaying table')
        throw error
        
      }
      response.status(200).json(results.rows)
    })

}
 updateStudentdata = (request,response) =>{
      console.log("update") 
      const name = request.params.schoolname
      const id = request.params.id
      const student_name = request.body.student_name
      const location = request.body.location
      const languages = request.body.languages
      console.log(student_name)
      const updatestudentdata =`UPDATE ${name}.studentdetails SET student_name ='${student_name}',location ='${location}',languages = '${languages}'  WHERE student_id = ${id};`
      client.query(updatestudentdata,(error,results) => {
        if(error) { 
          response.status(500).json('error in updating data')
          throw error
         }
         response.status(200).json("updated sucessfully")
      })
 }
 
 deleteStudent=(request,response) =>{
  console.log("delete") 
  const name = request.params.schoolname
  const id = request.params.id
  const deletesqlstudentdata =`DELETE FROM ${name}.studentdetails WHERE student_id =${id};`
   client.query(deletesqlstudentdata,(error,results)=>{
     console.log(deletesqlstudentdata)
     if(error){
      response.status(500).json('error in deleting table')
       throw error
     }

     response.status(200).json("deleted sucessfully")
   })
 }
 dropschool=(request,response) =>{
   console.log("drop school")
   const name = request.params.schoolname
   const drop = `DROP SCHEMA ${name} CASCADE`
   client.query(drop,(error,results)=>{
     console.log("deleting ur school")
     if(error){
       response.status(500).json(`error in deleting school`)
       throw error
     }
     response.status(200).json(`The ${name} school sucessfully dropped`)
   })
 }
 backup=(request,Response) => {
   console.log(`preparing for backup the table`)
   const name = request.params.schoolname
   const backupdata =`INSERT INTO public.backup SELECT * , '${name}' FROM ${name}.studentdetails;`
   client.query(backupdata,(error,results)=>{
    console.log("backing up")
    if(error){
     Response.status(500).json(`error can't backup table`)
      throw error
    }

    Response.status(200).json("backup completed sucessfully")
  })

 }
 backupStudent=(request,Response) => {
  console.log(`preparing for backup`)
  const name = request.params.schoolname
  const id = request.params.id
  const backupdata =`INSERT INTO public.backup SELECT * , '${name}' FROM ${name}.studentdetails WHERE student_id =${id};`
  client.query(backupdata,(error,results)=>{
   console.log("backing up")
   if(error){
    Response.status(500).json(`error can't backup table`)
     throw error
   }

   Response.status(200).json("backup completed sucessfully")
  
 })

}

module.exports = {
   createSchool,
   createRegister,
   addStudent,
   displaySchool,
   displayStudentdata,
   displayStudent,
   displayBackupdata,
   updateStudentdata,
   deleteStudent,
   dropschool,
   backup,
   backupStudent
  }
